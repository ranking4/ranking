SPACE = " "

def parse_line(line: str) -> list:
    name, *marks = line.strip().split(SPACE)
    marks = [int(_) for _ in marks]
    return [name] + marks

def load_data(mark_file: str) -> list:
    with open(mark_file, 'r') as file:
        return [parse_line(line) for line in file]

def compare_students(student1, student2):
    return all(m1 > m2 for m1, m2 in zip(student1[1:], student2[1:]))

def group_students(students):
    groups = []
    
    while students:
        current_student = students.pop(0)
        new_group = [current_student]
        remaining_students = []
        
        for student in students:
            if compare_students(current_student, student):
                new_group.append(student)
            else:
                remaining_students.append(student)
        
        groups.append(new_group)
        students = remaining_students
    
    return groups

def format_output(groups):
    output = []
    
    for group in groups:
        names = [student[0] for student in group]
        if len(names) > 1:
            output.append(' < '.join(sorted(names)))
        else:
            output.append(names[0])
    
    return '\n'.join(output)


file_path = 'marks.txt'
students = load_data(file_path)
groups = group_students(students)
result = format_output(groups)
print(result)



