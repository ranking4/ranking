def compare_students(marks1, marks2):
    if all(m1 > m2 for m1, m2 in zip(marks1, marks2)):
        return 1
    elif all(m2 > m1 for m1, m2 in zip(marks1, marks2)):
        return -1
    else:
        return 0

def read_student_marks(filename):
    students_marks = []
    with open(filename, 'r') as file:
        for line in file:
            parts = line.strip().split()
            student_name = parts[0]
            marks = list(map(int, parts[1:]))
            students_marks.append((student_name, marks))
    return students_marks

def find_comparable_pairs(students_marks):
    pairs = []
    for i in range(len(students_marks)):
        for j in range(i + 1, len(students_marks)):
            result = compare_students(students_marks[i][1], students_marks[j][1])
            if result == 1:
                pairs.append((students_marks[i][0], students_marks[j][0]))
            elif result == -1:
                pairs.append((students_marks[j][0], students_marks[i][0]))
    return pairs




filename = 'marks.txt'
students_marks = read_student_marks(filename)

if len(students_marks) >= 2:
    pairs = find_comparable_pairs(students_marks)
    
    for u, v in pairs:
        print(f"{u} > {v}")
    
   




